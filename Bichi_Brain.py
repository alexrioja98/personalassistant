from GoogleSTT import googleRecognition
from GoogleTTS import TTS
from Module_Calendar import GoogleCalendar
import Module_Interface_Database as db
import time
import pandas as pd
import random
from thread_recognize import Thread_Recognize


class Bichi_Brain:
    def __init__(self):
        self.gCalendar = GoogleCalendar()
        self.tts = TTS()

        self.months = (
            "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre",
            "Octubre",
            "Noviembre", "Diciembre")

    def process_text(self, text, event, recognizer):
        persona, emo_fisher, emo_dnn = recognizer.join()
        context = event.query_result.action
        if 'calendario' in context:
            if 'calendario.get_events' in context:
                events = self.gCalendar.get_events()
                if len(events) == 0:
                    self.tts.generate_file("Vaya, no tiene eventos cercanos en su calendario")
                for event in events:
                    # print(event)
                    try:
                        if 'dateetime' in event['start']:
                            date_time = event['start']['dateTime']
                            day = date_time[8:10]
                            month = date_time[5:7]
                            texto = "Tiene :" + event['summary'] + " el " + str(day) + " de " + self.months[
                                int(month) - 1]
                            self.tts.generate_file(texto)
                        elif 'date' in event['start']:
                            date_time = event['start']['date']
                            day = date_time[8:10]
                            month = date_time[5:7]
                            texto = "Tiene :" + event['summary'] + " el " + str(day) + " de " + self.months[
                                int(month) - 1]
                            self.tts.generate_file(texto)
                    except:
                        self.tts.generate_file("No se ha podido recuperar el evento")
            elif 'calendario.fecha' in context:
                event_fields = event.query_result.parameters.fields
                try:
                    # print(event_fields)
                    fecha = str(event_fields['fecha'])
                    titulo = str(event_fields['Titulo'])
                    fecha = fecha[0:len(fecha) - 8]
                    titulo = titulo[0:len(titulo) - 1]
                    fecha_array = fecha.split("string_value: \"")
                    titulo_array = titulo.split("string_value: \"")
                    fecha = fecha_array[1]
                    titulo = titulo_array[1]
                    # print("GUARDANDO "+ fecha, titulo)
                    if self.gCalendar.insert_event(titulo, fecha):
                        text = text.replace("2020-04-29", "dia 29 de Abril")
                        self.tts.generate_file(
                            text)
                except:
                    self.tts.generate_file("No se ha podido completar la acción, por favor, siga las instrucciones.")
            else:
                self.tts.generate_file(text)
        elif 'note' in context:
            event_fields = event.query_result.parameters.fields
            if 'note.contenido' in context:
                response = event.query_result.fulfillment_text
                res = response.split(" su nota ")
                title = res[1]
                content = str(event_fields['Contenido'])
                content = content[15:len(content) - 2]
                db.insertNote(title.lower(), content)
                self.tts.generate_file(text)
            elif 'note.read' in context:
                if 'note.read.last' in context:
                    self.tts.generate_file(text)
                    response = db.getNote("")
                    if response is not None:
                        self.tts.generate_file("Su nota es, : " + str(response))
                    else:
                        self.tts.generate_file("No hemos encontrado notas")
                else:
                    title = str(event_fields['Titulo'])
                    title = title[0:len(title) - 2]
                    title_array = title.split("string_value: \"")
                    title = title_array[1]
                    # print("TITTTTTTULO: "+str(title))
                    response = db.getNote(title.lower())
                    if response is not None:
                        self.tts.generate_file("Su nota es, : " + str(response))
                    else:
                        self.tts.generate_file("No hemos encontrado notas con ese título")
            elif 'note.delete' in context:
                title = str(event_fields['Titulo'])
                title = title[0:len(title) - 2]
                title_array = title.split("string_value: \"")
                title = title_array[1]
                if db.removeNote(title.lower()):
                    self.tts.generate_file("Se ha borrado la nota con titulo; " + str(title))
                else:
                    self.tts.generate_file("Esa nota no existe, no ha sido creada")
            else:
                self.tts.generate_file(text)
        elif 'time' in context:
            if 'user.time.what_time_is_it' in context:
                hour = time.strftime("%H:%M")
                self.tts.generate_file("Por supuesto, son las " + str(hour))
            elif 'user.time.what_day_is_it' in context:
                day = time.strftime("%d")
                month = time.strftime("%m")
                self.tts.generate_file("Claro, hoy es " + str(day) + " de " + self.months[int(month) - 1])
        elif 'hello' in context or 'goodnight' in context or 'nice_to_talk_to_you' in context \
                or 'goodmorning' in context or 'goodevening' in context or 'beautiful' in context \
                or 'thank_you' in context or 'what_time_is_it' in context:
            self.tts.generate_file(text + persona)
        else:
            self.tts.generate_file(text)

        return persona, emo_fisher, emo_dnn

    def make_mem_tests(self):

        STT = googleRecognition()
        self.tts.generate_file(
            "Vale, a continuación le voy a realizar un test muy cortito que no tardará nada en resolver.")
        self.tts.generate_file("Simplemente tendrá que responder a unas pocas preguntas. Allá vamos!")
        test_num = random.randint(1, 3)
        mem_test_df = pd.read_csv('recursos/mem_test_' + str(test_num) + '.csv', header=0)
        questions = mem_test_df[mem_test_df.columns[0]].tolist()
        answers = []
        emo_fisher = []
        emo_dnn = []
        for q in questions:
            recognizer = Thread_Recognize()
            recognizer.start()
            self.tts.generate_file(q)
            text = STT.listen()
            # text = input("resp: ")
            while text is None or len(text) == 0:
                self.tts.generate_file("Perdone, ¿Puede repetir la respuesta?")
            answers.append(text)
            _, e_f, e_d = recognizer.join()
            emo_fisher.append(e_f)
            emo_dnn.append(e_d)
            if questions[len(questions) - 1] != q:
                self.tts.generate_file("Vale, siguiente pregunta.")

        self.tts.generate_file("Muchas gracias, ya hemos terminado el test. Tenga un buen día!")
        return questions, answers, test_num, emo_fisher, emo_dnn

    def play_game_1(self):
        STT = googleRecognition()
        rnd_nums = random.sample(range(20), 5)
        uper_num = random.randint(1, 19)
        while max(rnd_nums) < uper_num:
            rnd_nums = random.sample(range(20), 5)
            uper_num = random.randint(1, 19)

        self.tts.generate_file("Vale, esto va de la siguiente manera, le voy a decir 5 números y me tendrá que decir"
                               "cuáles están por encima del número que yo le indique. Tiene dos intentos. Empezamos!")

        self.tts.generate_file("Los números son: " + str(rnd_nums))
        self.tts.generate_file("Cuál de estos números está por encima del " + str(uper_num) + '?')
        stop = False
        iter=0
        text = STT.listen()
        said_numbers = [int(s) for s in text.split() if s.isdigit()]
        while iter<2 and not stop:
            for n in said_numbers:
                if n in rnd_nums:
                    if n <= uper_num:
                        self.tts.generate_file('Vaya, pruebe otra vez. Recuerde mayor que ' + str(uper_num))
                        continue
                    else:
                        self.tts.generate_file('Bien!. Lo ha hecho de maravilla')
                        stop = True
                        break
                else:
                    self.tts.generate_file(
                        'Diga un número de los que le he dicho antes. Recuerde mayor que ' + str(uper_num))
                    iter-=1
            iter+=1

        if not stop:
            self.tts.generate_file('Vaya, otra vez irá mejor!')
        return stop, iter, 'num_game'