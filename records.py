import pandas as pd
import spacy
from nltk.stem.snowball import SnowballStemmer
from pymongo import MongoClient
import datetime


class analyze_text:
    def __init__(self):
        path_2_dict = "recursos/SA/clean.csv"
        sentiment_dictionary = pd.read_csv(path_2_dict, skiprows=1, encoding="ISO-8859-1", engine='python')
        self.stemwords = sentiment_dictionary[sentiment_dictionary.columns[0]].tolist()
        self.sentiment = sentiment_dictionary[sentiment_dictionary.columns[1]].tolist()
        print(self.stemwords)
        print(self.sentiment)
        self.spacy_spanish_core = spacy.load('es_core_news_sm')
        self.stemmer = SnowballStemmer(language='spanish')
        mongo_cli = MongoClient(
            'mongodb+srv://<username>:<pwd>@clusterata-qha9m.mongodb.net/test?retryWrites=true&w=majority')
        db = mongo_cli.ATA
        self.record_col = db.record
        self.test_col=db.mem_tests
        self.game_col = db.games

    def pre_process_phrase(self, phrase):
        # Creating spacy doc to work with
        spacy_doc = self.spacy_spanish_core(phrase)
        # Toknization of the phrase, cleaning punctuation and stopwords
        words = [t.orth_ for t in spacy_doc if not t.is_punct | t.is_stop]
        # Cleaning unnecesary tokens (y, para, como...)
        lexical_tokens = [t.lower() for t in words if len(t) > 3 and
                          t.isalpha()]
        # Stemming words
        stems = [self.stemmer.stem(token) for token in lexical_tokens]
        return stems

    def extract_sentiment_phrase(self, phrase):
        phrase_pre_processed = self.pre_process_phrase(phrase)
        sentiment = 0
        for word in phrase_pre_processed:
            if word in self.stemwords:
                if self.sentiment[self.stemwords.index(word)] == 'positive':
                    sentiment += 1
                else:
                    sentiment -= 1
        self.save2mongo_phrase(phrase, sentiment)
        return sentiment

    def save2mongo_phrase(self, phrase, sentiment):
        ts = datetime.datetime.utcnow()
        insert_dic = {
            "date": ts,
            "type": "phrase",
            "phrase": phrase,
            "sentiment": sentiment
        }
        self.record_col.insert_one(insert_dic)

    def save2mongo_im(self, id, val_fisher, val_dnn):
        ts = datetime.datetime.utcnow()
        v_f = 0
        v_d = 0
        if val_fisher == 'feliz':
            v_f = 1
        elif val_fisher == 'triste':
            v_f = -1
        else:
            v_f=0
        if val_dnn == 'feliz':
            v_d = 1
        elif val_dnn == 'triste':
            v_d = -1
        else:
            v_d = 0
        v_total = v_f + v_d
        insert_dic = {
            "date": ts,
            "type": "image",
            "identity": id,
            "sentiment_fisher": val_fisher,
            "sentiment_dnn": val_dnn,
            "sentiment_num_total": v_total
        }
        self.record_col.insert_one(insert_dic)

    def save2mongo_test(self, questions, answers, num_test, emo_fisher, emo_dnn):
        ts = datetime.datetime.utcnow()
        v_total=[]
        for e_f, e_d in zip(emo_fisher, emo_dnn):
            if e_f == 'feliz':
                v_f = 1
            elif e_f == 'triste':
                v_f = -1
            else:
                v_f= 0
            if e_d == 'feliz':
                v_d = 1
            elif e_d == 'triste':
                v_d = -1
            else:
                v_d = 0
            v_t = v_f + v_d
            v_total.append(v_t)
        doc_test={
            "ts":ts,
            "test_num":num_test,
            "questions": questions,
            "answers":answers,
            "emotion_recognition_fisher":emo_fisher,
            "emotion_recognition_dnn":emo_dnn,
            "total_emotion_recognition":v_total
        }
        self.test_col.insert_one(doc_test)

    def save2mongo_game(self, game_id, score, passed):
        ts = datetime.datetime.utcnow()
        doc_game={
            'ts':ts,
            'game_id':game_id,
            'passed':passed,
            'score':score
        }
        self.game_col.insert_one(doc_game)