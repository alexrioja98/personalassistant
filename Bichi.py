import sys
from BichiBot import *
from Bichi_Brain import Bichi_Brain
from records import analyze_text
from thread_recognize import Thread_Recognize
from GoogleSTT import googleRecognition


def wait4input():
    # print("Presione alguna tecla para continuar(hacer de hotword)")
    input()


bichi_brain = Bichi_Brain()
model = "recursos/modelos/modelo.pmdl"
bichibot = BichiBot_DF()
analyzer = analyze_text()
# nlp = None
# detector = l4h(model)
while True:
    STT = googleRecognition()
    # detector = l4h(model)
    # stays here until it hears "Hola Bichi"
    # detector.listen()
    text = None
    recognizer = Thread_Recognize()
    recognizer.start()
    while text is None or len(text) == 0:
        text = STT.listen()
        # text=input("Introduzca texto: ")
    # print("Por favor, escriba a continuación:")
    # text = input()
    # if we want to turn Bichi off->
    if "salir" == text:
        sys.exit()
    elif "Realizar tests" == text:
        recognizer.join()
        q, a, n, e_f, e_d=bichi_brain.make_mem_tests()
        analyzer.save2mongo_test(q, a, n, e_f, e_d)
    elif "juego" ==text:
        r, s, g=bichi_brain.play_game_1()
        analyzer.save2mongo_game(g, s, r)
    else:
        # print("Pasamos a procesarlo")
        # nlp = tp.Natural_Language_Processing(text, False)

        # wait4input()
        analyzer.extract_sentiment_phrase(text)
        response, event = bichibot.get_response(text)
        print("     BichiBot: " + response)
        p, v_f, v_d=bichi_brain.process_text(response, event, recognizer)
        analyzer.save2mongo_im(p, v_f, v_d)

        # wait4input()
        # nlp= tp.Natural_Language_Processing
