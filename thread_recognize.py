from threading import Thread
import cv2
import pickle
import imutils
import numpy as np


class Thread_Recognize(Thread):
    def __init__(self):
        """
        initializes this class by loading the models and detector
        """
        # Call the Thread class's init function
        Thread.__init__(self)
        self._ret_name = ""
        self._ret_emo_fisher = ""
        self._ret_emo_dnn = ""
        # Modificar las siguientes variables al gusto del directorio montado
        embed_model = "resources/model/openface_nn4.small2.v1.t7"
        recognizer_path = "resources/pickle/recognizer.pickle"

        recognizer_emo_path = "resources/pickle/recognizer_emo.pickle"

        # face_detection_model_path = "resources/model"
        model_proto_path = "resources/model/deploy.prototxt"
        model_path = "resources/model/res10_300x300_ssd_iter_140000.caffemodel"

        self.detector = cv2.dnn.readNetFromCaffe(model_proto_path, model_path)
        self.embedder = cv2.dnn.readNetFromTorch(embed_model)
        with open("resources/pickle/labels.pickle", "rb") as f:
            inv_labels = pickle.load(f)
            self.labels_recog = {v: k for k, v in inv_labels.items()}
        with open("resources/pickle/labels_emotion.pickle", "rb") as f:
            inv_labels = pickle.load(f)
            self.labels_emotion = {v: k for k, v in inv_labels.items()}
        self.recognizer_emo= pickle.loads(open(recognizer_emo_path, "rb").read())
        self.recognizer_face = pickle.loads(open(recognizer_path, "rb").read())
        self.fisher_recognizer = cv2.face.FisherFaceRecognizer_create()
        self.fisher_recognizer.read("resources/trainer_fisher.yml")

    # Override the run() function of Thread class
    def run(self):
        """
        We perform facial recognition inside run
        :return:
        """
        try:
            self.video_interface = cv2.VideoCapture(0, cv2.CAP_DSHOW)
            name = ""
            ret, frame = self.video_interface.read()
            frame = imutils.resize(frame, width=600)
            (h, w) = frame.shape[:2]

            # construimos el blob desde la imagen
            imageBlob = cv2.dnn.blobFromImage(
                cv2.resize(frame, (300, 300)), 1.0, (300, 300))

            self.detector.setInput(imageBlob)
            faces = self.detector.forward()

            # loopeamos las caras que encontremos
            for i in range(0, faces.shape[2]):
                # sacamos la confianza (probabilidad) de cada predicción
                confidence = faces[0, 0, i, 2]

                # filtramos falsos positivos con nivel bajo de confianza
                if confidence > 0.5:
                    # coordenadas de la cara detectada
                    box = faces[0, 0, i, 3:7] * np.array([w, h, w, h])
                    (startX, startY, endX, endY) = box.astype("int")

                    face = frame[startY:endY, startX:endX]
                    gray = cv2.cvtColor(face, cv2.COLOR_BGR2GRAY)
                    id_, confidence = self.fisher_recognizer.predict(cv2.resize(gray, (64, 64)))
                    self._ret_emo_fisher=self.labels_emotion[id_]
                    try:
                        faceBlob = cv2.dnn.blobFromImage(face, 1.0 / 255,
                                                         (96, 96), (0, 0, 0), swapRB=True, crop=False)
                        self.embedder.setInput(faceBlob)
                        vec = self.embedder.forward()
                    except:
                        print("Cara no centrada en el campo de vision de cámara. Frame corrupto")

                    preds = self.recognizer_face.predict_proba(vec)[0]
                    j = np.argmax(preds)
                    proba = preds[j]
                    name = self.labels_recog[j]
                    print(name)
                    if proba * 100 > 50:
                        emotion = self.labels_emotion[j]
                        self._ret_emo_dnn=emotion
                    self._ret_name = name
            self.video_interface.release()
        except:
            pass

    def join(self, *args):
        """
        Overwrite join method to get the return value
        :param args:
        :return:
        """
        Thread.join(self, *args)
        return self._ret_name, self._ret_emo_fisher, self._ret_emo_dnn

# t = Thread_Recognize()
#
# input("presiona cosa")
# name = t.start()
# print(name)
# print(t.join())
