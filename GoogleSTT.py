import speech_recognition as sr
import sys


class googleRecognition():
    """
    This class works as an inteface with GoogleSTT services
    """
    r = None

    def listen(self):
        """
        Open the microphone and retrieves the GoogleSTT prediction or "" if error
        :return: str, prediction or "" if error
        """
        global r
        audio = None
        text = None
        with sr.Microphone() as source:
            # print("Estamos calibrando los parametros de ruido ambiente...")
            # r.adjust_for_ambient_noise(source,
            #                            duration=0.4)  # listen for 0.5 second to calibrate the energy threshold for ambient noise levels
            # print("Hablando que es gerundio \(apaga el sistema, salir\)->")
            print("Estoy listo!")
            audio = r.listen(source)
            # print("Termina el audio")
        # now we try to get the transcription of the audio
        try:
            text = r.recognize_google(audio, language="es-ES")
            text=str(text)
            if 'Vichy' in text:
                text =text.replace("Vichy", "Bichi")
            elif 'Richi' in text:
                text =text.replace("Richi", "Bichi")
            print("   Creo que dijiste: " + text)
            if "apaga el sistema" in text:
                print("APAGANDO EL SISTEMA POR COMPLETO")
                sys.exit()
            return text
        except sr.UnknownValueError:
            print("No se pudo procesar. ¿Has dicho algo?")
            return ""
        except sr.RequestError as e:
            print("Could not request results from Google Speech Recognition service; {0}".format(e))
            return ""

    def __init__(self):
        """
        It retrieves the recognizer, to be able to open the stream with the microphone later
        """
        global r
        r = sr.Recognizer()
