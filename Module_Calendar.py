from __future__ import print_function
import datetime
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request


# If modifying these scopes, delete the file token.pickle.
class GoogleCalendar:
    def __init__(self):
        """
        Creates calendar service and returns a reference to the first calendar.
        Code from Google Calendar API: https://developers.google.com/calendar/quickstart/python
        """
        SCOPES = ['https://www.googleapis.com/auth/calendar', 'https://www.googleapis.com/auth/calendar.events']
        creds = None
        # The file token.pickle stores the user's access and refresh tokens, and is
        # created automatically when the authorization flow completes for the first
        # time.
        if os.path.exists('token.pickle'):
            with open('token.pickle', 'rb') as token:
                creds = pickle.load(token)
        # If there are no (valid) credentials available, let the user log in.
        if not creds or not creds.valid:
            if creds and creds.expired and creds.refresh_token:
                creds.refresh(Request())
            else:
                flow = InstalledAppFlow.from_client_secrets_file(
                    'google_cred_calendar.json', SCOPES)
                creds = flow.run_local_server(port=0)
            # Save the credentials for the next run
            with open('token.pickle', 'wb') as token:
                pickle.dump(creds, token)

        self.service = build('calendar', 'v3', credentials=creds)

        calendars = self.service.calendarList().list().execute()
        # print(calendars)
        # self.default_calendar=calendars['items'][0]

        #     As I already know my google calendar id, i just use it
        self.default_calendar_id = '1et3ode2k74bu921ge341tv8n0@group.calendar.google.com'

    def get_events(self, date_from='', date_to=''):
        now = datetime.datetime.utcnow().isoformat() + 'Z'  # 'Z'
        if date_to != '':
            events = self.service.events().list(calendarId=self.default_calendar_id,
                                                maxResults=3, timeMax=date_to, timeMin=now, singleEvents=True).execute()
        else:
            events = self.service.events().list(calendarId=self.default_calendar_id,
                                                maxResults=3, timeMin=now, singleEvents=True).execute()
        print("EVENTS"+str(events))
        return events.get('items', [])

    def insert_event(self, title, date):
        event = {
            'summary': title[0:len(title)],
            'location': 'Hyderabad',
            'description': 'MI vs TBD',
            'start': {
                'dateTime': date,
                'timeZone': "Europe/Madrid",
            },
            'end': {
                'dateTime': date,
                'timeZone': "Europe/Madrid",
            },
            'reminders': {
                'useDefault': False,
                'overrides': [
                    {'method': 'email', 'minutes': 24 * 60},
                    {'method': 'popup', 'minutes': 10},
                ],
            },
        }
        try:
            self.service.events().insert(calendarId=self.default_calendar_id, body=event).execute()
            return True
        except:
            return False


#
# def main():
#     """Shows basic usage of the Google Calendar API.
#     Prints the start and name of the next 10 events on the user's calendar.
#     """
#     creds = None
#     # The file token.pickle stores the user's access and refresh tokens, and is
#     # created automatically when the authorization flow completes for the first
#     # time.
#     if os.path.exists('token.pickle'):
#         with open('token.pickle', 'rb') as token:
#             creds = pickle.load(token)
#     # If there are no (valid) credentials available, let the user log in.
#     if not creds or not creds.valid:
#         if creds and creds.expired and creds.refresh_token:
#             creds.refresh(Request())
#         else:
#             flow = InstalledAppFlow.from_client_secrets_file(
#                 'google_cred_calendar.json', SCOPES)
#             creds = flow.run_local_server(port=0)
#         # Save the credentials for the next run
#         with open('token.pickle', 'wb') as token:
#             pickle.dump(creds, token)
#
#     service = build('calendar', 'v3', credentials=creds)
#
#     # Call the Calendar API
#     now = datetime.datetime.utcnow().isoformat() + 'Z' # 'Z' indicates UTC time
#     print('Getting the upcoming 10 events')
#     events_result = service.events().list(calendarId='primary', timeMin=now,
#                                         maxResults=10, singleEvents=True,
#                                         orderBy='startTime').execute()
#     events = events_result.get('items', [])
#
#     if not events:
#         print('No upcoming events found.')
#     for event in events:
#         start = event['start'].get('dateTime', event['start'].get('date'))
#         print(start, event['summary'])
#
#
# if __name__ == '__main__':
#     main()
