import snowboy.snowboydecoder as snowboydecoder


# Listen input for the given hotword and return True when find
class Listen4hotword:
    detector = None
    interrupted = None
    numPlayers = None

    def interrupt_callback(self):
        global interrupted
        return interrupted

    def okay(self):
        global interrupted
        interrupted = True
        snowboydecoder.play_audio_file()

    def listen(self, trivial=False, answer=False):
        global detector
        print('Escuchando... Ctrl+C para salir')
        detector.start(detected_callback=self.okay,
                       interrupt_check=self.interrupt_callback,
                       sleep_time=0.5)
        # return
        detector.terminate()
        return True

    def __init__(self, model, trivial=False):
        global detector
        global interrupted
        interrupted = False
        detector = snowboydecoder.HotwordDetector(model, sensitivity=0.40)