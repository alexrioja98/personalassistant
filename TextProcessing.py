
from os import remove
import os.path
import threading
import spacy
import random
# from spacy import displacy

##REALIZAMOS EL IMPORT DE LOS MODULOS DE SAZ
# import module_Music as Mta
import pruebas_youtube as yt
import module_RAE as buscador
import module_wiki as wiki
import GoogleTTS as TTS
import module_persons as Persons
import module_Trivial as trivial

text = ""


class Natural_Language_Processing:

    # extraer las personas de la frase y llamar al módulo de procesamiento en caso necesario
    def analisisPersonas(self, document, intencion):
        personas = [ent.text for ent in document.ents if ent.label_ == "PER"]
        personas_str = ','.join(personas)
        print(personas_str)
        pers_conoc = []
        pers_descon = []
        if len(personas) > 0:
            if (intencion == 'guardar'):
                print("habria que guardar a " + personas_str)
                for p in personas:
                    if Persons.consultarPersona(p):
                        pers_conoc.append(p)
                    else:
                        pers_descon.append(p)
                        Persons.guardarPersona(p)

                if len(pers_conoc) > 0 and len(pers_descon) > 0:
                    TTS.generarArchivo(str(pers_conoc) + " os conozco. A " + str(
                        pers_descon) + " mi no conocer, yo soy Saz, encantada", "prueba", True)
                elif len(pers_conoc) > 0 and len(pers_descon) == 0:
                    if len(pers_conoc) == 1:
                        if p == 'Alejandro':
                            TTS.hablandoQueEsGerundio("meInput")
                        else:
                            c = random.randrange(3)
                            if c == 0:
                                TTS.generarArchivo("Vaya vaya " + p + ", ¿Acaso no nos conocíamos?", "prueba", True)
                            if c == 1:
                                TTS.generarArchivo("Es extraño pero creo que a mis circuitos les suena tu nombre" + p,
                                                   "prueba", True)
                            if c == 2:
                                TTS.generarArchivo("¿No nos conocíamos" + p + "? De aquella fiesta en ... ese sitio?",
                                                   "prueba", True)
                    else:
                        TTS.hablandoQueEsGerundio("conozcoAlguien")
                elif len(pers_conoc) == 0 and len(pers_descon) > 0:
                    if len(pers_descon) > 1:
                        c = random.randrange(3)
                        if c == 0:
                            TTS.generarArchivo("Hola " + personas_str + " encantada de conoceros, yo soy Saz", "prueba",
                                               True)
                        if c == 1:
                            TTS.generarArchivo("Hola " + personas_str + " ¿qué tal estáis? Mi nombre es Saz. ",
                                               "prueba", True)
                        if c == 2:
                            TTS.generarArchivo("A mi me llaman Saz. Me alegro de conoceros " + personas_str, "prueba",
                                               True)
                    else:
                        c = random.randrange(3)
                        if c == 0:
                            TTS.generarArchivo("Hola " + personas_str + " encantada de conocerte, yo soy Saz", "prueba",
                                               True)
                        if c == 1:
                            TTS.generarArchivo("Hola " + personas_str + " ¿qué tal?. Mi nombre es Saz. ", "prueba",
                                               True)
                        if c == 2:
                            TTS.generarArchivo("A mi me llaman Saz. Me alegro de conocerte " + personas_str, "prueba",
                                               True)
            elif (intencion == ''):
                pass

    def __init__(self, t, test):
        global text
        text = str(t)
        ###############PRUEBAS CON LA LIBERIA SPACY##############################
        # Lematización para obtener los lemmas (raices lexicas derivativas) con spacy
        sp = spacy.load('es_core_news_sm')
        document = sp(text)
        tokens_sp = [t.orth_ for t in document if not t.is_punct | t.is_stop]
        verbos = [token.lemma_ for token in document if token.pos_ == "VERB"]
        print(verbos)
        print(tokens_sp)

        if 'conocer' in verbos or 'presentar' in verbos or 'presentarte' in tokens_sp or 'conozcas' in tokens_sp:
            try:
                self.analisisPersonas(document, 'guardar')
            except:
                TTS.hablandoQueEsGerundio("error")
        elif 'reproducir' in verbos or 'reproducir' in tokens_sp:
            print("yeyeye")
            try:
                busqueda = text.replace("Reproducir", "")
                yt.playFromYoutube(busqueda)
            # except:
            # TTS.hablandoQueEsGerundio("error")
            finally:
                remove("song.m4a")
        elif 'buscar' in verbos or 'significar' in verbos or 'significado' in tokens_sp:
            # buscador.RAE_scrapper()
            try:
                significado = buscador.RAE_scrapper(tokens_sp[len(tokens_sp) - 1])
                TTS.generarArchivo(significado, "prueba", True)
            except:
                TTS.hablandoQueEsGerundio("error")
        elif 'saber' in verbos:
            resumen = wiki.infoWiki(tokens_sp[len(tokens_sp) - 1])
            if len(resumen) == 0:
                TTS.generarArchivo("No se han encontrado datos sobre eso", "prueba", True)
            else:
                TTS.generarArchivo(resumen, "prueba", True)
        elif 'juego' in tokens_sp or 'jugar' in verbos:
            trivial.iniciar()
        else:
            TTS.hablandoQueEsGerundio("disculpa")
            print("no te he entendido")