import sqlite3

database_path = 'recursos/notas.db'


def getNote(title):
    """
    Used in order to retrieve a note by the given title or
    the first note if title==""
    :param title:
    :return:
    """
    conn = sqlite3.connect(database_path)
    cursor = conn.cursor()
    if title == "":
        cursor.execute(
            "SELECT titulo,contenido FROM notas"
        )
    else:
        cursor.execute(
            "SELECT titulo,contenido FROM notas WHERE titulo=\"" + title + "\""
        )
    conn.commit()

    rows = cursor.fetchall()
    conn.close()
    for row in rows:
        return row
    return None


def insertNote(title, content):
    """
    Used to insert a note into the notes database
    :param title: title of the note
    :param content: content of the note
    :return:
    """
    conn = sqlite3.connect(database_path)
    cursor = conn.cursor()
    cursor.execute(
        "INSERT INTO NOTAS (titulo,contenido) VALUES ('" + str(title) + "','" + str(content) + "')")
    conn.commit()
    conn.close()


def removeNote(title):
    """
    Given a Note title, searches for it in the database and deletes it
    :param title:
    :return:
    """
    if getNote(title) is None:
        return False
    else:
        conn = sqlite3.connect(database_path)
        cursor = conn.cursor()
        cursor.execute(
            "DELETE FROM notas WHERE titulo=\"" + title + "\"")
        conn.commit()
        conn.close()
        return True
