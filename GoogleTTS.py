import playsound
from gtts import gTTS
import os

class TTS:
	def __init__(self, route='recursos/audios/'):
		"""
		Creates the class that is used in order to make calls to STT API and play audio files from system
		:param route: route of the 'caché' or place where the audios are placed
		"""
		self.route=route

	def generate_file(self, text, nombre='temp', reproducir=True):
		"""
		Creates a file containing an audio response, this is usefull to load the file later on
		from the system instead of making a call to GoogleSTT API (less response time)
		:param text:  text of the generated audio file
		:param nombre: name of the generated audio file
		:param reproducir: True if you want to play it, False to just generate it
		:return: None
		"""
		tts = gTTS(text=text, lang='es-es')
		nombre = self.route + nombre
		tts.save(nombre + ".mp3")
		if reproducir:
			self.talking_que_es_gerundio(nombre, False)
			os.remove(nombre+ ".mp3")

	def talking_que_es_gerundio(self, nombre='temp', PonerRuta=True):
		"""
		It plays a specific file from the system
		:param nombre: name of the file to play
		:param PonerRuta: True if name does not contains the full path, False if full path specified
		:return: None
		"""
		#print(nombre)
		if (PonerRuta):
			nombre = self.route + nombre
			filename = nombre + '.mp3'
			playsound.playsound(filename)
		else:
			nombre = nombre + ".mp3"
			playsound.playsound(nombre)

