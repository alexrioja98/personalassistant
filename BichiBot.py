from chatterbot import ChatBot
from chatterbot.trainers import ListTrainer, ChatterBotCorpusTrainer
import dialogflow
from google.api_core.exceptions import InvalidArgument
import os


class BichiBot:
    """
    This class is the interface with Chatterbot, it will load all the possible conversations and then
    use them in order to get the prediction of an input text
    """

    def __init__(self):
        """
        Creates a Bichibot object and trains it with the given data
        """
        self.bichibot = ChatBot("Bichi Asistente")
        # conversacion=["hola que tal?", "bien y vos?", "naranja"]
        trainer = ChatterBotCorpusTrainer(self.bichibot)
        trainer.train("chatterbot.corpus.spanish.greetings")

    def get_response(self, text):
        """
        It get's the prediction for a given input text
        :param text: input text that you want to get the response of
        :return: str, the prediction
        """
        return str(self.bichibot.get_response(text))


class BichiBot_DF:
    """
    This class is the interface with DialogFlow.
    """

    def __init__(self):
        """
        Creates a session with Google´s Dialogflow.
        NEED credential .json file !!!!
        """
        os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = 'google_cred_dialogflow.json'
        self.DIALOGFLOW_PROJECT_ID = 'small-talk-pmfuuj'
        self.DIALOGFLOW_LANGUAGE_CODE = 'es'
        self.SESSION_ID = 'Alejandro'
        self.session_client = dialogflow.SessionsClient()
        self.session = self.session_client.session_path(self.DIALOGFLOW_PROJECT_ID, self.SESSION_ID)

    def get_response(self, text):
        """
        Given the input text, retrieves information about the predicction(response)
        :param text: input
        :return: response from dialogflow
        """
        text_input = dialogflow.types.TextInput(text=text, language_code=self.DIALOGFLOW_LANGUAGE_CODE)
        query_input = dialogflow.types.QueryInput(text=text_input)
        try:
            response = self.session_client.detect_intent(session=self.session, query_input=query_input)
        except InvalidArgument:
            raise
        #print(response)
        #print("Action triggered:"+str(response.query_result.action))
        #print("Detected intent confidence:", response.query_result.intent_detection_confidence)
        #return '('+str(response.query_result.intent_detection_confidence)+')-'+response.query_result.fulfillment_text, response
        return response.query_result.fulfillment_text, response